﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailGun : MonoBehaviour {
    public Animation laserAnim;
    public GameObject aimer;
    public GameObject satellite;
    public GameObject projectile;
    public float force;
	// Use this for initialization
	void Awake () {
        laserAnim.Play();
        satellite = GameObject.FindGameObjectWithTag("Satellite");
	}
	
	// Update is called once per frame
	void Start () {
        takeAim();
	}

    void LookAt2D(Transform lookingTransfrom)
    {
        Vector3 direction = satellite.transform.position- lookingTransfrom.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        lookingTransfrom.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }
    void takeAim()
    {
        LookAt2D(aimer.transform);
    }
    public void Fire()
    {
        GameObject bullet = Instantiate(projectile, aimer.transform.position, aimer.transform.rotation);
        Rigidbody2D bulletRB = bullet.GetComponent<Rigidbody2D>();
        bulletRB.velocity = bullet.transform.up * force;

    }
}
