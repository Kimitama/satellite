﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OutroStory : MonoBehaviour {

    int soundReduction = 3;
    int letterIndex;

    string one = "The satellite was quickly destroyed once inside, but it managed to send pictures of aliens back";
    string two = "Their technology is incredibly advanced, and they're a threat beyond our wildest imagination.";
    string three = "All countries have begun working together towards a common goal: protecting The Earth.";
    string four = "We would never have known, was it not for you.";
    string five = "Thank you, for giving humanity a chance of survival.";

    AudioSource typingSound;

    public int storySegment;

    public float timeBetweenLetters;

    public Text dialogueText1;
    public Text dialogueText2;
    public Text dialogueText3;
    public Text dialogueText4;
    public Text dialogueText5;

    public Text continueText;
    float wait;


    float time;
    // Use this for initialization
    void Awake()
    {
        typingSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (storySegment)
        {
            case 0:
                wait = 2;
                time = 0;
                storySegment++;
                break;
            case 1:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 2:
                
                for (int i = 0; i < one.Length; i++)
                {
                    char character = one[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText1));
                }
                storySegment++;
                time = 0;
                wait = one.Length * timeBetweenLetters;
                break;
            case 3:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 4:
                continueText.gameObject.SetActive(true);
                if (Input.GetKeyDown("space"))
                {
                    storySegment++;
                    continueText.gameObject.SetActive(false);
                }
                break;
            case 5:
                for (int i = 0; i < two.Length; i++)
                {
                    char character = two[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText2));
                }
                storySegment++;
                time = 0;
                wait = two.Length * timeBetweenLetters;
                break;
            case 6:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 7:
                continueText.gameObject.SetActive(true);
                if (Input.GetKeyDown("space"))
                {
                    storySegment++;
                    continueText.gameObject.SetActive(false);
                }
                break;
            case 8:
                for (int i = 0; i < three.Length; i++)
                {
                    char character = three[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText3));
                }
                storySegment++;
                time = 0;
                wait = three.Length * timeBetweenLetters;
                break;
            case 9:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 10:
                continueText.gameObject.SetActive(true);
                if (Input.GetKeyDown("space"))
                {
                    storySegment++;
                    continueText.gameObject.SetActive(false);
                }
                break;
            case 11:
                for (int i = 0; i < four.Length; i++)
                {
                    char character = four[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText4));
                }
                storySegment++;
                time = 0;
                wait = four.Length * timeBetweenLetters;
                break;
            case 12:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 13:
                continueText.gameObject.SetActive(true);
                if (Input.GetKeyDown("space"))
                {
                    storySegment++;
                    continueText.gameObject.SetActive(false);
                }
                break;
            case 14:
                for (int i = 0; i < five.Length; i++)
                {
                    char character = five[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText5));
                }
                storySegment++;
                time = 0;
                wait = five.Length * timeBetweenLetters;
                break;
            case 15:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 16:
                continueText.gameObject.SetActive(true);
                continueText.text = "Press 'space' to return to main menu";
                if (Input.GetKeyDown("space"))
                {
                    storySegment++;
                    continueText.gameObject.SetActive(false);
                }
                break;
            case 17:
                SceneManager.LoadScene(0);
                storySegment++;
                break;



        }
    }
    IEnumerator WriteLetterDelayed(char character, float delay, Text text)
    {
        yield return new WaitForSeconds(delay);
        text.text = text.text + character;
        letterIndex--;
        if (letterIndex <= 0)
        {
            typingSound.pitch = Random.Range(0.95f, 1.05f);
            typingSound.Play();
            letterIndex = soundReduction;
        }
    }

}
