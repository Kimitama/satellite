﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Score : MonoBehaviour {
    public int lvl = -1;
    public int[] secondsLeft;
    public int[] obstaclesHit;
	// Use this for initialization
	void Awake () {
        DontDestroyOnLoad(gameObject);
        secondsLeft = new int[2];
        obstaclesHit = new int[2];
    }

    // Update is called once per frame
    public void initiate()
    {
        lvl = SceneManager.GetActiveScene().buildIndex - 2;
        obstaclesHit[lvl] = 0;

    }
    public void HitObstacle()
    {
        obstaclesHit[lvl]++;
    }
    public void SubmitSecondsLeft(int seconds)
    {
        secondsLeft[lvl] = seconds;
        GameObject.FindGameObjectWithTag("ObsScore").GetComponent<Text>().text = "" + obstaclesHit[lvl];
        GameObject.FindGameObjectWithTag("SecScore").GetComponent<Text>().text = "" + secondsLeft[lvl];
        
    }
}
