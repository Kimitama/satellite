﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RailGunFire : MonoBehaviour {
    public RailGun railGun;
	// Use this for initialization
    public void Fire()
    {
        railGun.Fire();
    }
}
