﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowSatelliteX : MonoBehaviour {
    public GameObject satellite;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector2 (satellite.transform.position.x, transform.position.y);

    }
}
