﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetScores : MonoBehaviour {

    public Text time1;
    public Text time2;
    public Text time3;

    public Text hits1;
    public Text hits2;
    public Text hits3;

    public Score score;

    // Use this for initialization
    void Awake () {

        score = GameObject.FindGameObjectWithTag("Score").GetComponent<Score>();

        time1.text = "" + score.secondsLeft[0];
        time2.text = "" + score.secondsLeft[1];
        int totalSeconds = score.secondsLeft[0] + score.secondsLeft[1];
        time3.text = "" + totalSeconds;

        hits1.text = "" + score.obstaclesHit[0];
        hits2.text = "" + score.obstaclesHit[1];
        int totalHits = score.obstaclesHit[0] + score.obstaclesHit[1];
        hits3.text = "" + totalHits;

        Destroy(score.gameObject);
        Destroy(GameObject.FindGameObjectWithTag("Music"));
    }

    // Update is called once per frame
    void Update () {
		
	}
}
