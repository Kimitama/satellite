﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilingBackground : MonoBehaviour {
    public GameObject[] backgrounds;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 position = transform.position;
        Vector2 backgroundPosition = backgrounds[0].transform.position;
        Vector2 corner = Vector2.zero;

        if (Mathf.Abs( position.y - backgroundPosition.y) > 15)
        {
            if (position.y < backgroundPosition.y)
            {
                backgrounds[0].transform.position += new Vector3(0,-15);
            }
            if (position.y > backgroundPosition.y)
            {
                backgrounds[0].transform.position += new Vector3(0, 15);
            }
            position = transform.position;
            backgroundPosition = backgrounds[0].transform.position;
            corner = Vector2.zero;
        }
        if (Mathf.Abs(position.x - backgroundPosition.x) > 15*1.6f)
        {
            if (position.x < backgroundPosition.x)
            {
                backgrounds[0].transform.position += new Vector3(-15 * 1.6f, 0);
            }
            if (position.x > backgroundPosition.x)
            {
                backgrounds[0].transform.position += new Vector3(15 * 1.6f, 0);
            }
            position = transform.position;
            backgroundPosition = backgrounds[0].transform.position;
            corner = Vector2.zero;
        }

        if (position.y < backgroundPosition.y)
        {
            placeVertical(-1);
            corner += new Vector2(0,-15);
        }
        if (position.y > backgroundPosition.y)
        {
            placeVertical(1);
            corner += new Vector2(0, 15);
        }
        if (position.x < backgroundPosition.x)
        {
            placeHorizontal(-1.6f);
            corner += new Vector2(-15*1.6f, 0);
        }
        if (position.x > backgroundPosition.x)
        {
            placeHorizontal(1.6f);
            corner += new Vector2(15 * 1.6f, 0);
        }
        placeCorner(corner);
    }
    void placeVertical(float mult)
    {
        backgrounds[1].transform.position = backgrounds[0].transform.position + new Vector3(0, 15*mult);
    }
    void placeHorizontal(float mult)
    {
        backgrounds[2].transform.position = backgrounds[0].transform.position + new Vector3(15*mult,0);
    }
    void placeCorner(Vector3 cornerPosition)
    {
        backgrounds[3].transform.position = backgrounds[0].transform.position + cornerPosition;
    }
}
