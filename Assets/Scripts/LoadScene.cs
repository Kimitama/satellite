﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {
    public GameObject musicPlayer;
    public GameObject score;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void loadSceneMusic(int index)
    {
        Instantiate(musicPlayer);
        Instantiate(score);
        SceneManager.LoadScene(index);

    }
    public void loadScene(int index)
    {
        SceneManager.LoadScene(index);

    }

}
