﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpritemapLoader : MonoBehaviour {
    object[] asteroidSprites;
    // Use this for initialization
    void Awake()
    {
        asteroidSprites = Resources.LoadAll("Asteroid");

    }
    public Sprite GetRandomSprite()
    {
        Sprite output = (Sprite)asteroidSprites[Random.Range(1, asteroidSprites.Length - 1)];
        return output;
    }
}
