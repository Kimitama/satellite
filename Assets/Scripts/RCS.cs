﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RCS : MonoBehaviour {

    public float power = 1;
    Rigidbody2D parentRB;
    int timesFired;
    ParticleSystem particleSystem;
    // Use this for initialization
    void Awake()
    {
        parentRB = GetComponentInParent<Rigidbody2D>();
        particleSystem = GetComponent<ParticleSystem>();
    }
	
	// Update is called once per frame
	void Update () {
	}

    void FixedUpdate()
    {
        if (timesFired == 0)
        {
            particleSystem.Stop();
        }
        timesFired = 0;
    }

    public void fire(float thrust)
    {
        //if (timesFired != 0) { Debug.LogWarning("This thruster has fired for the " + timesFired + " time"); }
        timesFired++;
        Vector3 direction = -transform.up;
        direction = direction * power * thrust;
        parentRB.AddForceAtPosition(direction,transform.position);
        if (!particleSystem.isEmitting)
        {
            particleSystem.Play();
        }
    }
}
