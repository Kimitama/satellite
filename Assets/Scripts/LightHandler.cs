﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightHandler : MonoBehaviour {
    public Sprite on;
    public Sprite off;

    public SpriteRenderer lightRenderer;

    public float BlinkTime;

    public float time;

    public float destruction;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (time >= BlinkTime)
        {
            lightRenderer.sprite = on;
            time = 0;
        }
        if (time >= BlinkTime - (destruction * BlinkTime))
        {
            lightRenderer.sprite = off;
        }
        time += Time.deltaTime;
	}
}
