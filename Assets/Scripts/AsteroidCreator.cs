﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidCreator : MonoBehaviour {
    public float spawnWidth;
    float startAltitude;
    float distance;
    public float minDistance;
    public float maxDistance;
    public float minScale;
    public float maxScale;
    public float minRotation;
    public float maxRotation;
    public float maxSpeed;
    public float minSpeed;
    public GameObject asteroidPrefab;

    [Header("Railguns")]
    public bool railguns;
    float timeR;
    public float minTimeR;
    public float maxTimeR;
    public GameObject railGunPrefab;
    float timeRMult = 1;

    [Header("Other")]
    public float safeZoneSize;

    public Telemetry telemetry;
    // Use this for initialization

    // Update is called once per frame
    void Update () {
		if (distance <= 0 && telemetry.altitudeKM > safeZoneSize)
        {
            createAsteroid();
            distance = Random.Range(minDistance,maxDistance);
        }
        if (railguns)
        {
            if (telemetry.altitudeKM <= 100 && timeRMult != 2)
            {
                timeRMult = 2;
            }
            if (timeR <= 0 && telemetry.altitudeKM > safeZoneSize && telemetry.altitudeKM < 200 -safeZoneSize)
            {
                createRailGun();
                timeR = Random.Range(minTimeR, maxTimeR);
            }
        }
        if (telemetry.difference > 0)
        {
            distance -= telemetry.difference;
            if (railguns)
            {
                timeR -= Time.deltaTime* timeRMult;
            }

        }
    }
    private void OnDrawGizmos()
    {
        Vector2 start = transform.position - new Vector3(spawnWidth, 0);
        Vector2 end = transform.position + new Vector3(spawnWidth, 0);
        Gizmos.DrawLine(start, end);
    }
    void createAsteroid()
    {
        GameObject asteroid;
        Vector3 position = transform.position + new Vector3(Random.Range(-spawnWidth, spawnWidth), 0);
        asteroid = Instantiate(asteroidPrefab, position, Quaternion.identity);
        Rigidbody2D asteroidRB = asteroid.GetComponent<Rigidbody2D>();
        asteroidRB.velocity = new Vector2(0, Random.Range(maxSpeed, minSpeed));
        asteroidRB.angularVelocity = Random.Range(minRotation, maxRotation);
        asteroid.transform.localScale = Vector2.one * Random.Range(minScale, maxScale);
    }
    void createRailGun()
    {
        GameObject railGun;
        Vector3 position = transform.position + new Vector3(Random.Range(-spawnWidth, spawnWidth), -10);
        railGun = Instantiate(railGunPrefab, position, Quaternion.identity);
    }

}
