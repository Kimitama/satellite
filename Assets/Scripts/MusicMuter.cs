﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicMuter : MonoBehaviour {
    AudioSource audioSource;
	// Use this for initialization
	void Awake () {
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown("m"))
        {

            audioSource.mute = !audioSource.mute;
        }
	}
}
