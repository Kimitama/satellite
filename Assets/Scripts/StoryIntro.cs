﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class StoryIntro : MonoBehaviour {
    int soundReduction = 3;
    int letterIndex;

    string burnInitiate = "Initiating burn for stable orbit around 'Planet Nine' in t-10 seconds 9... 8... 7... 6... 5... 4... 3... 2... 1...";
    string Burning = "Burning ion thruster............";
    string StableOrbit = "Probe 'Sol's last frontier' has been confirmed in a stable orbit around 'Planet Nine'!";
    string GreatTalk = "Finally, after 50 years of flying through space, we can gather data on the mysterious Planet Nine!";
    string UnidentifiedObject = "WARNING: UNIDENTIFIED OBJECT ON COLLISION COURSE";

    AudioSource typingSound;

    public Animation dialogueAnimation;
    public Animation satellite;
    public int storySegment;

    public string initiateBurn;

    public float timeBetweenLetters;

    public Text dialogueText;
    public Text continueText;
    float wait;

    public ParticleSystem ionThruster;
    public GameObject ionThrusterSprite;

    float time;
    // Use this for initialization
    void Awake () {
        typingSound = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        switch (storySegment)
        {
            case 0:
                dialogueAnimation.Play("Open");
                storySegment++;
                break;
            case 1:
                if (!dialogueAnimation.isPlaying)
                {
                    dialogueText.gameObject.SetActive(true);
                    storySegment++;
                }
                break;
            case 2:
                for (int i = 0; i < initiateBurn.Length; i++)
                {
                    char character = initiateBurn[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText));
                }
                storySegment++;
                time = 0;
                wait = initiateBurn.Length * timeBetweenLetters;
                break;
            case 3:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 4:
                continueText.gameObject.SetActive(true);
                if (Input.GetKeyDown("space"))
                {
                    storySegment++;
                    continueText.gameObject.SetActive(false);
                    dialogueText.text = "";
                    wait = 3f;
                    time = 0;
                }
                break;
            case 5:
                ionThrusterSprite.SetActive(true);
                ionThruster.Play();
                satellite.Play("slowDown");
                for (int i = 0; i < Burning.Length; i++)
                {
                    char character = Burning[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText));
                }
                storySegment++;
                break;
            case 6:
                if (time >= wait)
                {
                    ionThrusterSprite.SetActive(false);
                    ionThruster.Stop();
                    dialogueText.text = "";
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 7:
                for (int i = 0; i < StableOrbit.Length; i++)
                {
                    char character = StableOrbit[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText));
                }
                storySegment++;
                time = 0;
                wait = StableOrbit.Length * timeBetweenLetters;
                break;
            case 8:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 9:
                continueText.gameObject.SetActive(true);
                if (Input.GetKeyDown("space"))
                {
                    storySegment++;
                    continueText.gameObject.SetActive(false);
                    dialogueText.text = "";
                }
                break;
            case 10:
                for (int i = 0; i < GreatTalk.Length; i++)
                {
                    char character = GreatTalk[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText));
                }
                storySegment++;
                time = 0;
                wait = GreatTalk.Length * timeBetweenLetters;
                break;
            case 11:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 12:
                continueText.gameObject.SetActive(true);
                if (Input.GetKeyDown("space"))
                {
                    storySegment++;
                    continueText.gameObject.SetActive(false);
                    dialogueText.text = "";
                }
                break;
            case 13:
                dialogueText.color = Color.red;
                for (int i = 0; i < UnidentifiedObject.Length; i++)
                {
                    char character = UnidentifiedObject[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText));
                }
                storySegment++;
                time = 0;
                wait = UnidentifiedObject.Length * timeBetweenLetters;
                break;
            case 14:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;
            case 15:
                satellite.Play("RTG break");
                storySegment++;
                break;
            case 16:
                if (!satellite.isPlaying)
                {
                    storySegment++;
                    time = 0;
                    wait = 3;
                    dialogueText.text = "";
                    dialogueAnimation.Play("Close");
                }
                break;
            case 17:
                if (time >= wait)
                {
                    storySegment++;
                }
                time += Time.deltaTime;
                break;

            case 18:
                SceneManager.LoadScene(2);
                storySegment++;
                break;



        }
    }
    IEnumerator WriteLetterDelayed(char character, float delay, Text text)
    {
        yield return new WaitForSeconds(delay);
        text.text = text.text + character;
        letterIndex--;
        if (letterIndex <= 0)
        {
            typingSound.pitch = Random.Range(0.95f, 1.05f);
            typingSound.Play();
            letterIndex = soundReduction;
        }
    }

}
