﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAsteroidSprite : MonoBehaviour {

    Sprite asteroidSprite;
    public SpriteRenderer asteroid;

	// Use this for initialization
	void Start () {
        asteroidSprite = GameObject.FindGameObjectWithTag("AsteroidSpriteHandler").GetComponent<SpritemapLoader>().GetRandomSprite();
        asteroid.sprite = asteroidSprite;
	}
	
	// Update is called once per frame
}
