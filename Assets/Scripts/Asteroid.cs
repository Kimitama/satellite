﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {
    public GameObject asteroidExplosion;
    public GameObject SavedParticles;
    // Use this for initialization
    void Awake()
    {
        if (SavedParticles != null)
        {
            if (SavedParticles.transform.parent != null)
            {
                SavedParticles.transform.parent = null;
                Destroy(SavedParticles, 15);
                
            }
        }
    }

        // Update is called once per frame
        void Update () {
		if (SavedParticles != null)
        {
            SavedParticles.transform.position = transform.position;
        }
	}
    public void explode()
    {
        Destroy(gameObject);
        Instantiate(asteroidExplosion,transform.position,Quaternion.identity);
    }
    public void OnDestroy()
    {
        if (SavedParticles != null)
        {
            SavedParticles.GetComponent<ParticleSystem>().emissionRate = 0;
        }
    }

}
