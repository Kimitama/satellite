﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Telemetry : MonoBehaviour {


    public Rigidbody2D rb;

    public Slider minimap;

    public PowerHandler power;

    Vector3 previousPosition;

    public float maxAltitude;
    public float altitudeKM;
    public float verticalVelocity;
    public float timeToArrival;
    public float timeToShutdown;
    public float difference;

    public Text altitudeText;
    public Text velocityText;
    public Text ETAText;
    public Text shutdownETAText;
    public Text differenceText;

    // Use this for initialization
    void Awake()
    {
        maxAltitude = altitudeKM;
        rb = GetComponent<Rigidbody2D>();
    }

    void Start () {
        previousPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        difference = previousPosition.y - transform.position.y;

        altitudeKM -= difference;

        previousPosition = transform.position;

        float velocity = -rb.velocity.y;

        int ETA = Mathf.RoundToInt(altitudeKM / velocity);
        int shutdown = Mathf.RoundToInt(power.power / power.drainEachSecond);

        int ETAShutdownDifference = shutdown - ETA;
        string differenceString = "";

        minimap.value = altitudeKM / maxAltitude;

        if (altitudeKM < 0)
        {
            altitudeText.text = "Altitude: " + "?" + "km";
            velocityText.text = "Velocity: " + "?" + "km/s";
            ETAText.text = "ETA: " + "Arrived";
            shutdownETAText.text = secondsToTimer(Mathf.RoundToInt(power.power / power.drainEachSecond));
            differenceText.text = "Difference: " + "?";
        }
        else
        {
            if (ETAShutdownDifference > 2000 || ETA < 0)
            {
                ETAText.text = "ETA: N/A";
            }
            else if (ETAShutdownDifference <= 0)
            {
                differenceString = "-" + secondsToTimer(Mathf.Abs(ETAShutdownDifference));
                differenceText.color = Color.red;
            }
            else
            {
                differenceString = secondsToTimer(ETAShutdownDifference);
                differenceText.color = Color.green;
            }

            altitudeText.text = "Altitude: " + Mathf.RoundToInt(altitudeKM) + "km";
            velocityText.text = "Velocity: " + Mathf.RoundToInt(velocity) + "km/s";
            if (ETA > 2000 || ETA < 0)
            {
                differenceString = "N/A";
                differenceText.color = Color.red;
            }
            else
            {
                ETAText.text = "ETA: " + secondsToTimer(ETA);
            }
            shutdownETAText.text = secondsToTimer(Mathf.RoundToInt(power.power / power.drainEachSecond));
            differenceText.text = "Difference: " + differenceString;

        }

    }
    string secondsToTimer(int seconds)
    {
        int minutes = Mathf.FloorToInt(seconds / 60);
        
        int secondsInMinute = seconds - (60 * minutes);
        string output;
        if (secondsInMinute < 10)
        {
            output = minutes + ":0" + secondsInMinute;
        }
        else
        {
            output = minutes + ":" + secondsInMinute;
        }

        return output;
    }
}
