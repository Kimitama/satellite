﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerHandler : MonoBehaviour {
    public Slider powerBar;
    public float power;
    public float drainEachSecond;
    public LightHandler lightHandler;
    public bool running = false;
    float maxPower;

    public GameObject retryCanvas;
	// Use this for initialization
	void Start () {
        maxPower = power;
	}
	
	// Update is called once per frame
	void Update () {
        if (running)
        {
            if (power <= 0)
            {
                running = false;
                retryCanvas.SetActive(true);
                
            }
            power -= Time.deltaTime * drainEachSecond;
            powerBar.value = power / maxPower;
            lightHandler.destruction = 1 - power / maxPower;
        }
        else
        {
            lightHandler.lightRenderer.sprite = lightHandler.off;
        }
    }
    private void LateUpdate()
    {
        power = Mathf.Clamp(power, 0, maxPower);
    }
}
