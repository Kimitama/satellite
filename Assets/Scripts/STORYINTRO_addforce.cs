﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class STORYINTRO_addforce : MonoBehaviour {
    public Vector2 force;
    public float rotationalForce;

    // Use this for initialization
    void Start () {
        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        rb.AddForce(force,ForceMode2D.Impulse);
        rb.AddTorque(rotationalForce, ForceMode2D.Impulse);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
