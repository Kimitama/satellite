﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Level1Story : MonoBehaviour
{
    public int SoundReduction;
    int letterIndex;
    AudioSource typingSound;

    string pressSpace = "We lost our power supply! If we're quick, we can still fly close enough to the planet to get good data! About 200km down should do it!    To enable power and stabilizers, press space.";

    public int storySegment = 0;
    public Animation dialogueAnimation;
    public Text dialogueText;

    public float timeBetweenLetters;
    public Satellitecontroller satellite;
    public PowerHandler power;
    public GameObject disabler;
    // Use this for initialization
    void Awake()
    {
        typingSound = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (storySegment)
        {
            case 0:
                dialogueAnimation.Play("Open");
                storySegment++;
                break;
            case 1:
                if (!dialogueAnimation.isPlaying)
                {
                    dialogueText.gameObject.SetActive(true);
                    storySegment++;
                }
                break;
            case 2:
                for (int i = 0; i < pressSpace.Length; i++)
                {
                    char character = pressSpace[i];
                    StartCoroutine(WriteLetterDelayed(character, i * timeBetweenLetters, dialogueText));
                }
                storySegment++;
                break;
            case 3:
                if (Input.GetKeyDown("space"))
                {
                    disabler.SetActive(true);
                    storySegment++;
                    dialogueText.gameObject.SetActive(false);
                    dialogueAnimation.Play("Close");
                    satellite.disabled = false;
                    satellite.Stabilizers(false);
                    power.running = true;
                }
                break;

        }
    }
    IEnumerator WriteLetterDelayed(char character, float delay, Text text)
    {
        yield return new WaitForSeconds(delay);
        text.text = text.text + character;
        letterIndex--;
        if (letterIndex <= 0)
        {
            typingSound.pitch = Random.Range(0.95f, 1.05f);
            typingSound.Play();
            letterIndex = SoundReduction;
        }

    }
}
