﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour {
    public GameObject RTG;
    public Asteroid asteroid;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void selfDestruct()
    {
        RTG.GetComponent<SpriteRenderer>().enabled = false;
    }
    public void destroyAsteroid()
    {
        asteroid.explode();
    }
}
