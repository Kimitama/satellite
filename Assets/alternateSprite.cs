﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class alternateSprite : MonoBehaviour {
    SpriteRenderer spriteRenderer;
    public Sprite[] sprites;
    int spriteIndex;
    float time;
    public float delay;
	// Use this for initialization
	void Awake () {
        spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (time <= 0)
        {
            time = delay;
            spriteIndex++;
            if (spriteIndex >= sprites.Length)
            {
                spriteIndex = 0;
            }
            spriteRenderer.sprite = sprites[spriteIndex];
        }
        time -= Time.deltaTime;
	}
}
